# Weekly report generator

This is a script that generates Create:Editor or Create:Code Review weekly report. It simply takes all your open issues and generates the report. The reports are generated using handlebars templates that you can customize.

In the current version it works the best at the start of the milestone, when all the issues are open.

## Prerequisites

- Node 12 or higher

## Templates

The script uses handlebar template (`report_template.hbs` in the repository root) to generate the report. You can either create one yourself or copy one of the two templates with `npm` tasks (`npm run editor` or `npm run code-review`).

Examples:

- [editor_template.hbs](editor_template.hbs)
- [code_review_template.hbs](code_review_template.hbs)

The template context contains `date` with formatted string and `issues` array, which contains the [REST API response with a list of issues](https://docs.gitlab.com/ee/api/issues.html#list-issues)

## Run

```sh
npm i
export TOKEN=<read-api gitlab token>
# copy your team's template (editor_template.hbs or code_review_template.hbs)
npm run editor # or npm run code-review
npm start
```

produces

```md
## Status Update 2020-10-26

| Issue | Status | Description
| ------ | ------ | ------ |
| [Add VS Code Extension to GitLab Tech Stack](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/249) | :green_apple: | |
| [Create POC for MR review feature](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/243) | :green_apple: | |
| [Trainee FE maintainer (GitLab) - Tomas Vik](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8602) | :green_apple: | |
| [TS migration 2: migrate gitlab_service](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/220) | :green_apple: | |


### Other things

-
```
