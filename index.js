const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');

const GITLAB_URL = 'https://gitlab.com';

const token = process.env.TOKEN;

if (!token) {
  console.error('please add your GitLab personal access token to "TOKEN" environment variable');
  process.exit(1);
}

const fetchOptions = {
  headers: {
    Authorization: `Bearer ${token}`,
  },
};

const getIssues = async () => {
  const userResponse = await fetch(`${GITLAB_URL}/api/v4/user`, fetchOptions);
  const user = await userResponse.json();
  const issuesResponse = await fetch(
    `${GITLAB_URL}/api/v4/issues?assignee_id=${user.id}&state=opened&scope=all`,
    fetchOptions,
  );
  return issuesResponse.json();
};

const renderReport = (issues, date) => {
  const templateStr = fs
    .readFileSync(path.resolve(__dirname, 'report_template.hbs'))
    .toString('utf8');
  const template = Handlebars.compile(templateStr, { noEscape: true });

  return template({ issues, date });
};

const generateReport = async () => {
  const issues = await getIssues();
  const now = new Date();
  const formattedDate = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`;
  return renderReport(issues, formattedDate);
};

generateReport().then(console.log);
